<?php

namespace Roots\Sage\Extras;

use Roots\Sage\Setup;

function add_to_twig($twig) {
    /* this is where you can add your own fuctions to twig */
    $twig->addExtension(new Twig_Extension_StringLoader());
    $twig->addFilter(new Twig_SimpleFilter('youtube_thumb', 'youtube_thumbnail'));
    return $twig;
}
//add_filter('get_twig', 'add_to_twig');

function youtube_thumbnail($id){
  $youtube_embed_code = get_field('video_url', $id);
  $video_id = substr($youtube_embed_code, stripos($youtube_embed_code, 'embed/') + 6);
  $video_thumbnail_url = 'http://img.youtube.com/vi/'. $video_id .'/mqdefault.jpg';

  return $video_thumbnail_url;
}
//add_filter('generate_youtube_thumbnail', 'filter_generate_youtube_thumbnail');

if (function_exists('add_theme_support')) {
  add_theme_support('post-thumbnails');
  //images sizes
  add_image_size( 'gallery_image', 360, 285, TRUE );
}
/**
 * Add <body> classes
 */
function body_class($classes) {
  // Add page slug if it doesn't exist
  if (is_single() || is_page() && !is_front_page()) {
    if (!in_array(basename(get_permalink()), $classes)) {
      $classes[] = basename(get_permalink());
    }
  }

  // Add class if sidebar is active
  if (Setup\display_sidebar()) {
    $classes[] = 'sidebar-primary';
  }

  return $classes;
}
add_filter('body_class', __NAMESPACE__ . '\\body_class');

/**
 * Clean up the_excerpt()
 */
function excerpt_more() {
  return ' &hellip; <a href="' . get_permalink() . '">' . __('Continued', 'sage') . '</a>';
}
add_filter('excerpt_more', __NAMESPACE__ . '\\excerpt_more');

/**
 * add js vars to footer such as ajaxurl
 */
function add_js_vars(){
  print '<script type="text/javascript">var ajaxurl = \'' . admin_url('admin-ajax.php') . '\'; </script>';
}
add_action('wp_footer', __NAMESPACE__ . '\\add_js_vars');

function gallery_filter_callback(){
  global $wpdb; // this is how you get access to the database

  /* post gallery icons */
  $article_icon_url    = get_template_directory_uri() . '/assets/images/article_icon.png';
  $video_icon_url      = get_template_directory_uri() . '/assets/images/video_icon.png';

  $gallery = '';

  $slug = $_POST['slug'];

  $args = array(
    'showposts' => 6,
      'post_type' => array('article', 'video'),
      'tax_query' => array(
          array(
          'taxonomy' => 'content_theme',
          'field' => 'slug',
          'terms' => array($slug)
      ))
  );

  $gallery_items = get_posts($args);
  foreach ($gallery_items as $gallery_item){
    $gallery .= '<a class="gallery-item-post-url" href="' . get_permalink($gallery_item->ID) . '">
                    <article class="gallery-item">
                      <header class="gallery-item__image-holder">';
                        if($gallery_item->post_type == "article"){ 
                          $article_hero = get_field('article_hero_image', $gallery_item->ID);
                          $gallery .= '<img class="gallery-item__image" src="' . $article_hero['sizes']['gallery_image'] . '" />';
                        }
                        else{
                          $gallery .= '<img class="gallery-item__image" src="http://img.youtube.com/vi/N5lKx3gRpcc/hqdefault.jpg" />';
                        } 
                      $gallery .= '</header>
                      <footer class="gallery-item__content-holder">';
                        if($gallery_item->post_type == "article"){ 
                          $gallery .= '<img class="gallery-item__type-icon" src="' . $article_icon_url . '"/>';
                        }
                        else{
                          $gallery .= '<img class="gallery-item__type-icon" src="' . $video_icon_url . '"/>';
                        }
                        $gallery .= '<h2 class="gallery-item__title">' . get_the_title($gallery_item->ID) . '</h2>
                        <p class="galler-item__short-desc">' . get_field('short_description', $gallery_item->ID) . '</p>
                      </footer>
                  </article>
                </a>';
  }

  print $gallery;

  wp_die(); // this is required to terminate immediately and return a proper response
}
add_action( 'wp_ajax_gallery_filter_callback', __NAMESPACE__ . '\\gallery_filter_callback' );

<?php
use Roots\Sage\Setup;

/**
 * Timber
 */

class TwigSageTheme extends TimberSite {
    function __construct() {
        add_filter( 'timber_context', array( $this, 'add_to_context' ) );
        parent::__construct();
    }
    function add_to_context( $context ) {

        /* Menu */
        $context['footer_menu'] = new TimberMenu('footer');

        /* Site info */
        $context['site'] = $this;

        $context['home_url'] = get_home_url();

        /* have home page ad unit loaded*/
        $context['advertisment'] = new TimberPost(get_field('home_page_ad_unit', 4));

        /* header image */
        $context['header_image'] = new TimberImage(get_field('header_banner_image', 4));

        /* header image */
        $context['footer_logo_image_url'] = get_template_directory_uri() . '/assets/images/rogers_logo_footer.png';

        /* post gallery icons */
        $context['article_icon_url']    = get_template_directory_uri() . '/assets/images/article_icon.png';
        $context['video_icon_url']      = get_template_directory_uri() . '/assets/images/video_icon.png';

        return $context;

    }
}
new TwigSageTheme();

/**
 * Timber
 *
 * Check if Timber is activated
 */

if ( ! class_exists( 'Timber' ) ) {

    add_action( 'admin_notices', function() {
        echo '<div class="error"><p>Timber not activated. Make sure you activate the plugin in <a href="' . esc_url( admin_url( 'plugins.php#timber' ) ) . '">' . esc_url( admin_url( 'plugins.php' ) ) . '</a></p></div>';
    } );
    return;

}
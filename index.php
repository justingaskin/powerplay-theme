<?php
$context = Timber::get_context();

/*
	GENERIC video thumb
 */
$context['fallback_youtube_thumbnail'] = 'http://img.youtube.com/vi/N5lKx3gRpcc/hqdefault.jpg';

if(is_front_page()){
	/* Get taxonomy terms for filter*/
	$context['post_categories'] = Timber::get_terms('content_theme');
	
	//figure out what they want and ordering
	$first_cat_slug = $context['post_categories'][0]->slug;

	$args = array(
		'showposts' => 6,
	    'post_type' => array('article', 'video'),
	    'tax_query' => array(
	        array(
	        'taxonomy' => 'content_theme',
	        'field' => 'slug',
	        'terms' => array($first_cat_slug)
	    ))
    );

	$context['gallery_posts'] = Timber::get_posts($args);

	/* have home page ad unit loaded*/
    $context['featured_post'] = new TimberPost(get_field('home_page_featured_post', 4));
   
	Timber::render('templates/home.twig', $context);
}
else{
	Timber::render('templates/index.twig', $context);
}
<?php
$context = Timber::get_context();
$context['post'] = Timber::query_post();

//we check if the post is using a different ad than the home page if so load it.
if(!get_field('use_homepage_ad', $post->id)){
	
	$post_specific_ad = get_field('ad_unit', $post->id);
	//check if one was actually chosen may have been left blank by accident so keep homepage ad as default
	if($post_specific_ad){
		$context['advertisment'] = new TimberPost($post_specific_ad);
	}

}

Timber::render( array( 'templates/single-' . $post->ID . '.twig', 'templates/' . $post->post_type . '.twig', 'templates/single.twig' ), $context );